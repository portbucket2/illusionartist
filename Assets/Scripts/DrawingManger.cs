using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingManger : APBehaviour
{
    public Renderer paintingMeshRenderer, outputMeshRenderer;
    public Texture2D mainTexture;

    /// <summary>
    /// Hideable variable
    /// debug purpose public only
    /// </summary>
    public Texture2D modifiedTexture;
    public Material outputMeshMaterial;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        paintingMeshRenderer.material.mainTexture = mainTexture;
        outputMeshMaterial = outputMeshRenderer.material;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}




        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        outputMeshRenderer.sharedMaterial.SetTexture("PaintedTexture", paintingMeshRenderer.material.mainTexture);
        //outputMeshMaterial.mainTexture = paintingMeshRenderer.material.mainTexture;
        //outputMeshMaterial.color = paintingMeshRenderer.material.color;


        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
