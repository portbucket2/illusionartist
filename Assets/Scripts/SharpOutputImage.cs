using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharpOutputImage : APBehaviour
{
    public Renderer mainObjectRenderer;
    public Renderer outputRenderer;

    Material outputMaterial;
    Material mainObjectMaterial;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        mainObjectMaterial = mainObjectRenderer.material;

    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        outputMaterial = outputRenderer.material;
    }

    void Update()
    {

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        outputMaterial.mainTexture = mainObjectRenderer.material.mainTexture;
        outputMaterial.color = mainObjectRenderer.material.color;

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
